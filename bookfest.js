$(window).ready(function(){
	var pageWidth = $('body').width() > 920 ? 920 : $('body').width();	
	
	if($("#flipbook").length > 0) {
		$("#flipbook").turn({
			width:pageWidth,
			height: 580,
			autoCenter: true,
			duration: 1500
		});
	}
/* 	$("#flipbook").turn("page", 2); */
		
	$("#flipbook").bind("turning", function(event, page, view) {
		var totalPages = $("#flipbook").turn("pages")
	
		if( page == 1){
			$('#flipbook .p2').removeClass('fixed');			
		} else if ( page ==  totalPages ) {
			$('#flipbook .p'+ (totalPages-1)).removeClass('fixed');
		} else {
			$('#flipbook .p2, #flipbook .p'+(totalPages-1)).addClass('fixed');
			if ($('#instructions').is(":visible")) {
				$('#instructions').fadeOut();
			}
		}
		
		if (page == 10 || page == 11) {
			start_fancybox();
		}
	});
	
	$('#navigation .home_btn a').live('click', function (e) { e.preventDefault(); $('#flipbook').turn('page', 2);})
	$('#navigation .about_btn a').live('click', function (e) { e.preventDefault(); $('#flipbook').turn('page', 4);})
	$('#navigation .fun_btn a').live('click', function (e) { e.preventDefault(); $('#flipbook').turn('page', 6);})
	$('#navigation .authors_btn a').live('click', function (e) { e.preventDefault(); $('#flipbook').turn('page', 8);})
	$('#navigation .nolavie_contest_btn a').live('click', function (e) { e.preventDefault(); $('#flipbook').turn('page', 16);})
	$('#navigation .photos_btn a').live('click', function (e) {
		e.preventDefault();
		$('#flipbook').turn('page', 10);
		
		// reinitialzie fancybox when we turn to the photos page
		start_fancybox();
	})
	
	$('#navigation .partners_btn a').live('click', function (e) { e.preventDefault(); $('#flipbook').turn('page', 12);})
	$('#navigation .nolavie_contest_btn a').live('click', function (e) { e.preventDefault(); $('#flipbook').turn('page', 12);})

	function start_fancybox() {
		jQuery(".p10 a, .p11 a").fancybox({
			'cyclic': false,
			'autoScale': true,
			'padding': 10,
			'opacity': true,
			'speedIn': 500,
			'speedOut': 500,
			'changeSpeed': 300,
			'overlayShow': true,
			'overlayOpacity': "0.3",
			'overlayColor': "#666666",
			'titleShow': true,
			'titlePosition': 'inside',
			'enableEscapeButton': true,
			'showCloseButton': true,
			'showNavArrows': true,
			'hideOnOverlayClick': true,
			'hideOnContentClick': false,
			'width': 560,
			'height': 340,
			'transitionIn': "fade",
			'transitionOut': "fade",
			'centerOnScroll': true
		});
		
	}
}); //end onready fn()