<?php
/**
Template Name: Bookfest Home

 */

get_header(); ?>
<div id="instructions">Click here to open the book and begin the story!</div>
<div id="flipbook" class="shadow">
    <div class="hard own-size front_cover" style="width:480px; height:600px;"></div>
    <div class="hard own-size inside_front_cover" style="width:480px; height:600px;">
<!-- 	    <div class="depth"></div> -->
    </div>
    <div class="book-content full-page-image">
			<img src="<?php bloginfo('template_url'); ?>/images/img-bookfest.jpg">
    </div>    
    <div>
	    <div class="book-content">
	    	<h2>About</h2>
	    	<p>Join us for the 3rd Annual New Orleans Children's Book Festival on October 19 & 20, 2012. The free festival features storytelling, live music, food, and fun activities for kids of all ages!</p>
	    	<p>First Lady Cheryl Landrieu and Ruby Bridges both believe strongly in literacy and in the power of education. They also believe in the powerful effect that learning can have in bringing children together. This event affords children from all backgrounds the opportunity to get to know one another through the love of books and learning.</p>
	    	<p>The festival offers unique opportunities for children and parents to interact with exceptional writers and storytellers and to enjoy an inspiring variety of book-related activities, exhibitions and demonstrations.</p>
	    	
	    	<h4>The food, fun, books, and stories are FREE!</h4>
	    	<p>For more information, please contact <a href="mailto:info@rubybridgesfoundation.org">info@rubybridgesfoundation.org</a>.</p>   	
	    </div>
    </div>
	    <div id="about_page_frame" class="book-content full-page-image">
<!-- 				<img src="<?php bloginfo('template_url'); ?>/images/about-page-image.jpg"> -->
	    </div>
    <div>
	    <div class="book-content">
	    	<h3>Twinkle, Twinkle</h3>
	    	<h4>A Night of Music Inspired by Children's Literature</h4>
	    	<h4>Friday, October 19, 6:30 p.m. - 8:00 p.m</h4>
	    	<p>Twinkle, Twinkle is a special performance by the Greater New Orleans Youth Orchestras in celebration of the 3rd Annual Children's Book Festival. On the evening of Friday, October 19th, the Youth Orchestra will perform music inspired by children's literature.</p>
				<p>Bring picnic blankets and baskets for a night of music and magic under the stars!</p>
				
	    	<h4>Watch a Video of Twinkle Twinkle here:</h4>
	    	<div id="twinkle_video">
		    	<object width="380" height="220"><param name="allowfullscreen" value="true"></param><param name="movie" value="https://www.facebook.com/v/135910546510033"></param><embed src="https://www.facebook.com/v/135910546510033" type="application/x-shockwave-flash" allowfullscreen="1" width="380" height="220"></embed></object>
		    </div>
	  	</div>
    </div>
    <div>
	    <div class="book-content">
	    	<h3>New Orleans<br/>Children's Book Festival</h3>
	    	<h4>Saturday, October 20, 10:00 a.m. - 2:00 p.m.</h4>
	    	<div class="center_text">
		    	<p>Listen to stories read by your favorite authors...</p>
					<p>...Grab a sweet treat or tasty snack...</p>
					<p>...Play with a pony who has her own book...</p>
					<p>...Hang out with Clifford the Big Red Dog...</p>
					<p>...Get FREE Books!</p>
	    	</div>
				<p>These are but a few of the many fun activities you can enjoy at the New Orleans Children's Book Festival.</p>
	  	</div>
    </div>
    <div>
	    <div class="book-content author_page">
	    	<h2>Authors</h2>
	    	<p>The Children's Book Festival features book readings by some of New Orleans’ most beloved children’s story authors.  Check out this year’s featured authors and titles!</p>
			<!-- Start Author Images -->
				<img class="alignnone size-full wp-image-75" title="hoffman" src="<?php bloginfo('template_url'); ?>/images/author1.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-143" title="bookfest-nutcracker" src="<?php bloginfo('template_url'); ?>/images/author2.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-77" title="Sherry-'Leddy'-Milan1" src="<?php bloginfo('template_url'); ?>/images/author3.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-117" title="bookfest-nancy" src="<?php bloginfo('template_url'); ?>/images/author4.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-126" title="author-a" src="<?php bloginfo('template_url'); ?>/images/author5.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-140" title="bookfest-berthe" src="<?php bloginfo('template_url'); ?>/images/author6.jpg" alt="" width="122px" height="141px" />
			<!-- End Author Images -->	    
	    </div>
    </div>
    <div>
	    <div class="book-content author_page">
	    	<h2>Authors</h2>
			<!-- Start Author Images -->
				<img class="alignnone size-full wp-image-126" title="author-a" src="<?php bloginfo('template_url'); ?>/images/author7.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-141" title="bookfest-ruby" src="<?php bloginfo('template_url'); ?>/images/author8.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-126" title="author-a" src="<?php bloginfo('template_url'); ?>/images/author9.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-154" title="bookfest-freddy" src="<?php bloginfo('template_url'); ?>/images/author11.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-154" title="bookfest-freddy" src="<?php bloginfo('template_url'); ?>/images/author-cornell.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-154" title="bookfest-freddy" src="<?php bloginfo('template_url'); ?>/images/author-sue.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-154" title="bookfest-freddy" src="<?php bloginfo('template_url'); ?>/images/author-anthony.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-154" title="bookfest-freddy" src="<?php bloginfo('template_url'); ?>/images/author-happy.jpg" alt="" width="122px" height="141px" />
				<img class="alignnone size-full wp-image-154" title="bookfest-freddy" src="<?php bloginfo('template_url'); ?>/images/author-berthe.jpg" alt="" width="122px" height="141px" />


			<!-- End Author Images -->
			<p><strong><em>And many, many more authors and storytellers!</em></strong></p>	

	    </div>
    </div>
    <?php require_once('photo-gallery.php'); ?>
    <div>
	    <div class="book-content">
	    <h2>Win a School Visit</h2>
	    </div>
    </div>
    <div>
	    <div class="book-content">
	    <h2>Photos</h2>
	    </div>
    </div>    
    <div>
	    <div class="book-content">
	    <h2>Ruby Bridges</h2>
	    </div>
    </div>
    <div>
	    <div class="book-content">
	    <h2>Photos</h2>
	    </div>
    </div>    
    <div>
	    <div class="book-content nolavie">
		    <h2>NolaVie Contest</h2>
		    <p>NolaVie is partnering with the New Orleans' Children's Book Festival to sponsor
the New Orleans Children's Book Competition. We are accepting submissions,
illustrated or text-only, published, self-published, or unpublished, for reading levels
kindergarten through eighth grade.</p>
			<img align="right" src="<?php bloginfo('template_url'); ?>/images/nolavie-logo.png" alt="logo-nolavie" width="200" height="79" /><p>Award-winning books will be displayed at the festival itself on Saturday, October
20, at the Latter-Branch Library. Awards will be given for the best book, best
author, best illustrator, and more. Authors will also have an opportunity to read at
the festival and be featured on NolaVie.</p>
			<p>Submission deadline is October 1st in PDF format to <a href="mailto:anna@nolavie.com">anna@nolavie.com</a></p>
	    </div>
    </div>
    <?php require_once('partners.php'); ?>

    <div class="hard own-size inside_back_cover fixed" style="width:480px; height:600px;">
	    <div class="depth"></div>
    </div>
    <div class="hard own-size back_cover" style="width:480px; height:600px;"></div>
</div>
​
<?php get_footer(); ?>
