<?php
/**
 * @package WordPress
 * @subpackage Decubing
 */

get_header(); ?>


	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>
  <div id="<?php the_ID(); ?>" class="post">
    <?php if (is_front_page()){} else { ?>  <h1><span style="background:#fff; padding:0 20px;"><?php the_title(); ?></span></h1><?php } ?>
    	<?php the_content('Read the rest of this entry &raquo;'); ?>
  </div>
   		<?php endwhile; else : ?>
		<h1><span style="background:#fff; padding:0 20px;">Not Found</span></h1>
		<p>Sorry, but you are looking for something that isn't here.</p>
	<?php endif; ?>   

<?php get_footer(); ?>
