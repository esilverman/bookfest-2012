<?php
/**
 * @package WordPress
 * @subpackage Decubing
 */
?>
    <div id="navigation">
<?php
  $args = array(
  'orderby' => 'menu',
  'order' => 'ASC',
	'post_type' => 'page',
	'showposts' => 1000,
	'caller_get_posts' => 1
  ); 

$pageNum = 1; //Hompage for flipbook is page 2, 3
$pages = get_posts($args);
      foreach($pages as $page) {
      		$pageNum += 2;
          $out .= '';
          $out .=  '<a id="page_'.$pageNum.'_btn" class="nav_btn '.basename(get_permalink($page->ID)).'_btn" href="#'.get_permalink($page->ID).'" title="'.wptexturize($page->post_title).'">'.wptexturize($page->post_title).'</a>';
      }
    //echo $out;
?>    
<?php wp_nav_menu(); ?>
</div>  <!-- end #navigation -->

	</div> <!-- end #page -->
</div> <!-- end #wrapper -->
		<?php wp_footer(); ?>

</body>
</html>
