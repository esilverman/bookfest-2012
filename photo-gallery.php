<?php 
$galleryPage = get_page_by_title('Photo Gallery');
$photosArray = get_field('photo_gallery', $galleryPage->ID);
$imageNum = 0;
$IMAGES_PER_ROW = 4;
$NUM_ROWS = 4;
?>

<div>
	<div class="book-content">
		<h2>Photo Gallery</h2>
	  <table>
	  <tbody>
	    <?php for($i=0;$i<$IMAGES_PER_ROW*$NUM_ROWS ;$i++): ?>
	      <?php
	      $image = $photosArray[$i];
	      if ($imageNum++ % $IMAGES_PER_ROW == 0) :?>
	      	<tr>
	      <?php endif; ?>
	      		<td>
	          	<a href="<?php echo $image['url'] ;?>" rel="fancybox"><img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
	      		</td>
	      <?php if ($imageNum % $IMAGES_PER_ROW == 0) :?>
	      	</tr>
	      <?php endif; ?>
	    <?php endfor; ?>
	  </tbody>
	  </table>
	</div>
</div>
<div>
  <div class="book-content">
	  <table>
	  <tbody>
		<h2>Photo Gallery</h2>
	    <?php for($i=16;$i<=count($photosArray);$i++): ?>
	      <?php
	      $image = $photosArray[$i];
	      if ($imageNum++ % $IMAGES_PER_ROW == 0) :?>
	      	<tr>
	      <?php endif; ?>
	      		<td>
	          	<a href="<?php echo $image['url'] ;?>" rel="fancybox"><img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
	      		</td>
	      <?php if ($imageNum % $IMAGES_PER_ROW == 0) :?>
	      	</tr>
	      <?php endif; ?>
	    <?php endfor; ?>
	  </tbody>
	  </table>
  </div>
</div>
