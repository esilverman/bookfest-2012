<?php
register_nav_menus( array('nav_menu' => 'Main Navigation Menu') );

function the_slug($postID, $echo=true){
	global $post;
	$post = $postID;
  $slug = basename(get_permalink());
  do_action('before_slug', $slug);
  $slug = apply_filters('slug_filter', $slug);
  if( $echo ) echo $slug;
  do_action('after_slug', $slug);
  return $slug;
}

?>