    <div>
	    <div class="book-content partners-page">
	    	<h3>A special thanks to our sponsors and partners...</h3>
	   		<table>
		   		<tbody>
			   		<tr> 	
			   			<td class="img_wrapper">
					    	<img class="alignnone size-full wp-image-28" title="neworeans" src="http://nolabookfest.org/wp-content/uploads/2011/09/neworeans.jpg" alt="">
					    </td>
			  	    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-137" title="RBF logo" src="http://nolabookfest.org/wp-content/uploads/2011/09/RBF-logo-300x138.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-full wp-image-35" title="18272832343852565860_GNOYO" src="http://nolabookfest.org/wp-content/uploads/2011/09/18272832343852565860_GNOYO.jpeg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-45" title="fnopl_logo" src="http://nolabookfest.org/wp-content/uploads/2011/09/fnopl_logo-300x249.jpg" alt="">
					    </td>
			   		</tr>
			   		<tr>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-114" title="hudson_dufry_logo_positive" src="http://nolabookfest.org/wp-content/uploads/2011/09/hudson_dufry_logo_positive-300x271.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-57" title="notmc_blue" src="http://nolabookfest.org/wp-content/themes/nolabookfest-wptheme/images/nolavie-logo.png" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-113" title="WAG logo_tag_stacked there's a way" src="http://nolabookfest.org/wp-content/uploads/2011/09/WAG-logo_tag_stacked-theres-a-way-300x109.jpg" alt="">
					   </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-46" title="chevron" src="http://nolabookfest.org/wp-content/uploads/2011/09/chevron-281x300.jpg" alt="">
					    </td>
			   		</tr>
			   		<tr>

					    <td class="img_wrapper">
					    	<img class="alignnone size-full wp-image-27" title="louisiana" src="<?php bloginfo('template_url'); ?>/images/logo-free.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-39" title="First Book logo - large" src="http://nolabookfest.org/wp-content/uploads/2011/09/First-Book-logo-large-300x255.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-39" title="First Book logo - large" src="http://nolabookfest.org/wp-content/uploads/2011/09/print-logo.jpeg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-39" title="First Book logo - large" src="<?php bloginfo('template_url'); ?>/images/nolavie-logo.png" alt="">
					    </td>
			   		</tr>
		   		</tbody>
	   		</table>
	    </div>
    </div>
    
    
    <div>
	    <div id="small_partners" class="book-content partners-page">
	    <table>
	    	<tbody>
			   		<tr>
					    <td class="img_wrapper">
					    	<img class="alignnone size-full wp-image-37" title="City_Year_Louisiana-158x166" src="http://nolabookfest.org/wp-content/uploads/2011/09/City_Year_Louisiana-158x166.jpeg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-44" title="fm-color-vertical" src="http://nolabookfest.org/wp-content/uploads/2011/09/fm-color-vertical1-260x300.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-45" title="fnopl_logo" src="http://nolabookfest.org/wp-content/uploads/2011/09/fnopl_logo-300x249.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-47" title="JW_RGB" src="http://nolabookfest.org/wp-content/uploads/2011/09/JW_RGB_High_Resolution-RGB-300x131.jpg" alt="">
					    </td>
			   		</tr>
			   		<tr>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-48" title="leh_logo_color" src="http://nolabookfest.org/wp-content/uploads/2011/09/leh_logo_color-300x115.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-50" title="lib1" src="http://nolabookfest.org/wp-content/uploads/2011/09/lib1-300x122.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-51" title="Logo- NEW! (1)" src="http://nolabookfest.org/wp-content/uploads/2011/09/Logo-NEW-1-300x54.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-52" title="midcoast" src="http://nolabookfest.org/wp-content/uploads/2011/09/midcoast-300x86.jpg" alt="">
					    </td>
			   		</tr> 	
			   		<tr>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-54" title="new_orleans_hornets_logo-9939" src="http://nolabookfest.org/wp-content/uploads/2011/09/new_orleans_hornets_logo-9939-300x291.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-56" title="New-Orleans-Saints-Logo" src="http://nolabookfest.org/wp-content/uploads/2011/09/New-Orleans-Saints-Logo-244x300.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-58" title="octaviabookslogo1" src="http://nolabookfest.org/wp-content/uploads/2011/09/octaviabookslogo1-300x79.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-full wp-image-60" title="scholastic_logo" src="http://nolabookfest.org/wp-content/uploads/2011/09/scholastic_logo.jpeg" alt="">
					    </td>
			   		</tr> 	
			   		<tr>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-61" title="southerneagle" src="http://nolabookfest.org/wp-content/uploads/2011/09/southerneagle-300x284.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-medium wp-image-62" title="Untitled-21" src="http://nolabookfest.org/wp-content/uploads/2011/09/Untitled-21-300x67.jpg" alt="">
					    </td>
					    <td class="img_wrapper">
					    	<img class="alignnone size-thumbnail wp-image-121" title="STAIR Logo clean copy" src="http://nolabookfest.org/wp-content/uploads/2011/09/STAIR-Logo-clean-copy-214x160.jpg" alt="">
					    </td>
			   		</tr> 	
	    	</tbody>
	    </table>	    
	    </div>
	  </div>  	
  	