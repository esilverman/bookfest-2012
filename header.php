<?php
/**
 * @package WordPress
 * @subpackage Decubing
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>New Orleans Book Festival</title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<!-- <link href='http://fonts.googleapis.com/css?family=Lora:400,400italic' rel='stylesheet' type='text/css'> -->

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/all.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/turn.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/bookfest.js"></script>

<?php if(is_page('sponsors') || is_page('authors')){ ?>
<style type="text/css">
 img { margin: 10px;}

</style>
<?php } ?>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-26308015-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<?php wp_head(); ?>   
</head>

<body>
<!--
<div id="header">
    <a href="#"><img style="float:none;" src="<?php bloginfo('template_url'); ?>/images/logo.png" width="818" height="338" border="0" title="2nd Annual New Orleans Children’s Book Festival" /></a>
</div>
-->
<div id="wrapper">
	<div id="page">
